# docker-alpine-s3fs

Uses [s3fs](https://github.com/s3fs-fuse/s3fs-fuse) with Alpine Linux to mount an S3 bucket as a directory in the filesystem. Exposes files via NGiNX.

## Configuration

* S3_ACCESS_KEY
* S3_SECRET_KEY
* S3_URL
* S3_BUCKET
* MOUNTPOINT (defaults to `/mnt/s3fs/${S3_BUCKET}`)
* MOUNTPOINT_DEFAULT_ACL (defaults to `public-read`)

## Running with Docker

```bash
docker run -ti \
  -e S3_ACCESS_KEY=access_key \
  -e S3_SECRET_KEY=secret_key \
  -e S3_URL=https://s3.example.com:6443 \
  -e S3_BUCKET=my-bucket \
  -e MOUNTPOINT_DEFAULT_ACL=private \
  -v /my-nginx.conf:/etc/nginx/nginx/conf \
  --privileged \
  samcarpentier/nginx-s3fs:alpine-stable
```

**Note: Docker privileged mode is required. Otherwise, the following error will occur.**

```
fuse: device not found, try 'modprobe fuse' first
```
