FROM nginx:stable-alpine

ARG S3FS_VERSION=v1.85

RUN apk --update add \
  ca-certificates \
  fuse \
  alpine-sdk \
  automake \
  autoconf \
  libxml2-dev \
  fuse-dev \
  curl-dev \
  git \
  bash

RUN git clone https://github.com/s3fs-fuse/s3fs-fuse.git; \
  cd s3fs-fuse; \
  git checkout tags/${S3FS_VERSION}; \
  ./autogen.sh; \
  ./configure --prefix=/usr; \
  make; \
  make install; \
  rm -rf /var/cache/apk/*;

COPY setup-s3fs-mountpoint.sh /

CMD ["/bin/sh", "-c" , "/bin/sh /setup-s3fs-mountpoint.sh && nginx -g 'daemon off;'"]
